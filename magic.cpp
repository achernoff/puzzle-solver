/**
 * $Id: magic.cpp,v 1.1 2013/02/14 01:04:03 acc3863 Exp acc3863 $
 *
 * $Log: magic.cpp,v $
 * Revision 1.1  2013/02/14 01:04:03  acc3863
 * Initial revision
 *
 *
 * author: acc3863
 */

#include <iostream>
#include <sstream>
#include <set>
#include "Solver.h"
#include "MagicConfig.h"

#define REQ_ARGS 9
#define MAX_SQ 8
#define MIN_SQ 1

using namespace std;

typedef list< const Config* > VecCfg;

int main( int argc, const char* argv[] ) {
	if( argc == REQ_ARGS ) {
		int* goal(new int[ BOX_SIZE ]);
		bool badInput( false );
		set< int > used;
		for( int i( 0 ); i < BOX_SIZE; ++i ) {
			istringstream issn(argv[ i + 1 ]);
			int index = i < BOX_SIZE/2 ?
				i : BOX_SIZE+BOX_SIZE/2 - i - 1;
			issn >> goal[index];
			badInput = badInput || issn.fail() || !issn.eof() ||
				goal[index] > MAX_SQ || goal[index] < MIN_SQ ||
				used.find( goal[index] ) != used.end();
			used.insert( goal[index] );
		}
		if( badInput ) {
			cerr << "Invalid argument." << endl <<
				"Usage: ./magic # # # # # # # #" << endl;
		} else {
			MagicConfig::setGoal( goal );
			Solver solver;
			VecCfg path;
			int* init( new int[ BOX_SIZE ] );
			for( int i(0); i < BOX_SIZE; ++i )
				init[i]=i<BOX_SIZE/2?i+1:BOX_SIZE+BOX_SIZE/2-i;
			MagicConfig* initCfg(new MagicConfig(init,0,0));
			solver.solve( initCfg, path );
			if( !path.empty() ) {
				int stepCt( 1 );
				for( VecCfg::const_iterator it(++path.begin());
					it != path.end(); ++it, ++stepCt ) {
					cout<<"#"<<stepCt<<" transform ";
					if((*it) -> getMove()[0] == TRANS_A)
						cout << "A" << endl;
					else if((*it)->getMove()[0]==TRANS_B)
						cout << "B" << endl;
					else if((*it)->getMove()[0]==TRANS_C)
						cout << "C" << endl;
					(*it) -> print( cout );
					delete *it;
				}
			} else {
				cout << "No solution." << endl;
			}
			delete initCfg;
		}
		delete[] goal;
	} else {
		cerr << "Usage: ./magic # # # # # # # #" << endl;
	}
	return 0;
}
