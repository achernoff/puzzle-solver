/**
 * Version:
 * $Id: LightConfig.cpp,v 1.14 2013/02/07 01:53:15 acc3863 Exp $
 * 
 * Revisions:
 * $Log: LightConfig.cpp,v $
 * Revision 1.14  2013/02/07 01:53:15  acc3863
 * indexing typos and bound checking typos
 *
 * Revision 1.13  2013/02/07 01:19:13  acc3863
 * new macros, spelling
 *
 * Revision 1.12  2013/02/07 01:12:31  acc3863
 * fixed ifstream hanging pointer
 *
 * Revision 1.11  2013/02/07 00:22:46  acc3863
 * now toggles central light
 *
 * Revision 1.10  2013/02/07 00:19:12  acc3863
 * bug fixes
 *
 * Revision 1.9  2013/02/07 00:13:04  acc3863
 * altered indexing function
 * re-enabled light toggling
 *
 * Revision 1.8  2013/02/04 23:58:13  acc3863
 * implemented list optimizations discussed in class
 *
 * Revision 1.7  2013/02/04 23:18:47  acc3863
 * fixes to print function
 *
 * Revision 1.6  2013/02/03 18:29:37  acc3863
 * inlined functions
 * some optimizations
 *
 * Revision 1.5  2013/01/28 00:04:34  acc3863
 * improved some parts broke others
 *
 * Revision 1.4  2013/01/24 02:41:04  acc3863
 * new changes.
 *
 * Revision 1.3  2013/01/22 00:01:31  acc3863
 * first file reading impl
 *
 * Revision 1.2  2013/01/19 18:49:30  acc3863
 * added bounds checking and refactored some repeated code into functions
 *
 * Revision 1.1  2013/01/18 21:27:24  acc3863
 * Initial revision
 *
 *
 * author: acc3863
 */

#include <ios>
#include <cstring>
#include "LightConfig.h"

#define TOGGLE(x) x > 0 ? 0 : 1 // toggle light
#define ON_CHAR '*' // *
#define C_ON_CHAR '1' // 1
#define OFF_CHAR '.' // .
#define C_OFF_CHAR '0' // 0
#define ON_INT 1 // 1
#define C_ON_INT 2 // 2
#define OFF_INT 0 // 0
#define C_OFF_INT -1 // -1

using namespace std;

LightConfig::LightConfig( const int* dt, const int* mv, const Config* pr ):
	Config( dt, mv, pr ) {
}

LightConfig::~LightConfig() { }

void LightConfig::print( ostream &out ) const {
	for( int i( 0 ); i < HEIGHT; ++i ) {
		for( int j( 0 ); j < WIDTH; ++j ) {
			out << "[";
			if( getData()[ index( i, j ) ] == 1 ) {
				out << ON_CHAR;
			} else if( getData()[ index( i, j ) ] == 2 ) {
				out << C_ON_CHAR;
			} else if( getData()[ index( i, j ) ] == 0 ) {
				out << OFF_CHAR;
			} else if( getData()[ index( i, j ) ] == -1 ) {
				out << C_OFF_CHAR;
			} else {
				out << "?? ERROR ??";
			}
			out << "]";
		}
		out << endl;
	}
	out << endl;
}

const LightConfig* LightConfig::press( int x, int y ) const {
	int* nd(new int[ data_length() ]);
	int* nm(new int[ 2 ]);
	nm[ 0 ] = x;
	nm[ 1 ] = y;
	memcpy( nd, getData(), sizeof(int) * data_length() );
	if( boundChk( x + 1, y ) ) {
		nd[index( x + 1, y )] = TOGGLE(getData()[index( x + 1, y )]);
	}
	if( boundChk( x - 1, y ) ) {
		nd[index( x - 1, y )] = TOGGLE(getData()[index( x - 1, y )]);
	}
	if( boundChk( x, y + 1 ) ) {
		nd[index( x, y + 1 )] = TOGGLE(getData()[index( x, y + 1 )]);
	}
	if( boundChk( x, y - 1 ) ) {
		nd[index( x, y - 1 )] = TOGGLE(getData()[index( x, y - 1 )]);
	}
	if( boundChk( x, y ) ) {
		nd[index( x, y )] = TOGGLE(getData()[index( x, y )]);
	}
	return new LightConfig( nd, nm, this );
}

bool LightConfig::canPress( int x, int y ) const {
	bool xu( !boundChk( x + 1, y )
		|| getData()[ index( x + 1, y ) ] == 1
		|| getData()[ index( x + 1, y ) ] == 0 );
	bool xd( !boundChk( x - 1, y )
		|| getData()[ index( x - 1, y ) ] == 1
		|| getData()[ index( x - 1, y ) ] == 0 );
	bool yl( !boundChk( x, y + 1 )
		|| getData()[ index( x, y + 1 ) ] == 1
		|| getData()[ index( x, y + 1 ) ] == 0 );
	bool yr( !boundChk( x, y - 1 )
		|| getData()[ index( x, y - 1 ) ] == 1
		|| getData()[ index( x, y - 1 ) ] == 0 );
	bool sp( !boundChk( x, y )
		|| getData()[ index( x, y ) ] == 1
		|| getData()[ index( x, y ) ] == 0 );
	return xu && xd && yl && yr && sp;
}

LightConfig* LightConfig::parseStream( istream &in ) {
	int width( 1 );
	int height( 1 );
	in >> width >> height;
	LightConfig::WIDTH = width;
	LightConfig::HEIGHT = height;
	int cindex( 0 );
	int* nd ( new int[width * height] );
	while( !in.bad() && !in.eof() && cindex < width * height ) {
		char cur;
		in >> cur;
		if( cur == ON_CHAR ) {
			nd[ cindex ] = 1;
		} else if( cur == C_ON_CHAR ) {
			nd[ cindex ] = 2;
		} else if( cur == OFF_CHAR ) {
			nd[ cindex ] = 0;
		} else if( cur == C_OFF_CHAR ) {
			nd[ cindex ] = -1;
		} else {
			// uhh...
			//cerr << "Invalid character in input." << endl;
			break;
		}
		++cindex;
	}
	return new LightConfig( nd, 0, 0 );
}

void LightConfig::setGoal( int g ) {
	GOAL = g;
}

int LightConfig::WIDTH = 0;

int LightConfig::HEIGHT = 0;

int LightConfig::GOAL = 0;

