/**
 * $Id: ClockConfig.cpp,v 1.10 2013/02/06 18:15:40 scb3968 Exp $
 * 
 * $Log: ClockConfig.cpp,v $
 * Revision 1.10  2013/02/06 18:15:40  scb3968
 * changed data members to private and wrote setters and getters for each function.
 *
 * Revision 1.9  2013/02/04 23:58:13  acc3863
 * implemented list optimizations discussed in class
 *
 * Revision 1.8  2013/02/04 23:18:47  acc3863
 * fixes to print function
 *
 * Revision 1.7  2013/01/24 00:30:37  acc3863
 * standardized documentation
 * fixed RCS format
 * checked line size
 *
 * Revision 1.6  2013/01/23 05:45:25  acc3863
 * more inline
 *
 * Revision 1.5  2013/01/18 20:44:57  acc3863
 * successfully implemented inline
 *
 * Revision 1.4  2013/01/17 18:15:34  acc3863
 * refixed bounds bug
 *
 * Revision 1.3  2013/01/17 17:57:29  acc3863
 * fixed clock bounds error
 *
 * Revision 1.2  2013/01/17 17:32:09  acc3863
 * type fixes
 * bug fixes
 *
 * Revision 1.1  2013/01/17 16:48:54  acc3863
 * Initial revision
 *
 *
 * author: acc3863
 * author: scb3968
 */

#include "ClockConfig.h"
#include <ios>

using namespace std;

ClockConfig::ClockConfig( int* dt, int* mv, const Config* pr):
	Config( dt, mv, pr ) {

}

ClockConfig::~ClockConfig() { }

void ClockConfig::getChildren( list< const Config* > &childs ) const {
	int* cd1( new int[data_length()] );
	int* mv1( new int[1] );
	if( getData()[0] == CLOCK_SIZE ) {
		cd1[0] = 1;
	} else {
		cd1[0] = getData()[0] + 1;
	}
	mv1[0] = 0;

	int* cd2( new int[data_length()] );
	int* mv2( new int[1] );
	if( getData()[0] == 1 ) {
		cd2[0] = CLOCK_SIZE;
	} else {
		cd2[0] = getData()[0] - 1;
	}
	mv2[0] = 1;
	
	childs.push_back( new ClockConfig( cd1, mv1, this ) );
	childs.push_back( new ClockConfig( cd2, mv2, this ) );
}

void ClockConfig::print( ostream &out ) const {
	out << getData()[0];
}

int ClockConfig::getClockSize(){
	return CLOCK_SIZE;
}

void ClockConfig::setClockSize( int size ){
	CLOCK_SIZE = size;
}

int ClockConfig::getGoalHour(){
	return GOAL_HOUR;
}

void ClockConfig::setGoalHour( int goalHour ){
	GOAL_HOUR = goalHour;
}


int ClockConfig::GOAL_HOUR = 0;

int ClockConfig::CLOCK_SIZE = 0;
