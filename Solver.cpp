/**
 * $Id: Solver.cpp,v 1.13 2013/02/06 18:13:57 acc3863 Exp $
 * 
 * $Log: Solver.cpp,v $
 * Revision 1.13  2013/02/06 18:13:57  acc3863
 * new fixes
 *
 * Revision 1.12  2013/02/05 00:31:37  acc3863
 * debug changes
 *
 * Revision 1.11  2013/02/05 00:23:42  acc3863
 * more debug options
 *
 * Revision 1.10  2013/02/05 00:11:15  acc3863
 * more optimization
 *
 * Revision 1.9  2013/02/04 23:58:13  acc3863
 * implemented list optimizations discussed in class
 *
 * Revision 1.8  2013/01/24 00:30:37  acc3863
 * standardized documentation
 * fixed RCS format
 * checked line size
 *
 * Revision 1.7  2013/01/18 19:58:16  acc3863
 * experimented with inline functions
 *
 * Revision 1.6  2013/01/18 03:25:33  acc3863
 * added more const types
 *
 * Revision 1.5  2013/01/18 03:05:19  acc3863
 * removed debug function
 *
 * Revision 1.4  2013/01/18 02:59:34  acc3863
 * fixed set ordering issue
 *
 * Revision 1.3  2013/01/17 17:39:33  acc3863
 * added debug coce
 * code*
 *
 * Revision 1.2  2013/01/17 17:32:09  acc3863
 * type fixes
 * bug fixes
 *
 * Revision 1.1  2013/01/17 16:48:54  acc3863
 * Initial revision
 *
 *
 * author: acc3863
 * author: scb3968
 */

#ifdef DEBUG_1
#define DEBUG_0
#define DEBUG1(x) x
#else
#define DEBUG1(x)
#endif

#ifdef DEBUG_0
#define DEBUG0(x) x
#include <iostream>
#else
#define DEBUG0(x)
#endif

#include "Solver.h"
#include <set>
#include <queue>

using namespace std;

struct ConfigPtrComp {
	bool operator() ( const Config* const &lhs, const Config* const &rhs ) const {
		return (*lhs) < (*rhs);
	}
};

typedef list< const Config* > VecCfg;
typedef set< const Config*, ConfigPtrComp > SetCfg;
typedef queue< const Config* > QueueCfg;

Solver::Solver() { }

Solver::~Solver() { }

void Solver::solve( const Config * init, VecCfg &path ) const {
	SetCfg seen;
	QueueCfg cfgQ;
	VecCfg childs;
	bool endLoop( false );
	const Config* last = 0;

	seen.insert( init );
	cfgQ.push( init );

	DEBUG0(int collisions( 0 );)
	DEBUG0(int generated( 0 );)
	DEBUG1(int loopnum( 0 );)

	while( !cfgQ.empty() && !(cfgQ.front() -> isGoal()) && !endLoop) {
		DEBUG1(cout<<"Loop #"<<loopnum++<<endl;)
		DEBUG1(cout<<"Set Size: "<<seen.size()<<endl;)
		DEBUG1(cout<<"Que Size: "<<cfgQ.size()<<endl;)
		DEBUG1(int added( 0 );)

		cfgQ.front() -> getChildren( childs );
		cfgQ.pop();
		for( VecCfg::const_iterator it( childs.begin() ); it != childs.end(); ++it ) {
			DEBUG0(generated += 1;)
			if( seen.find( *it ) != seen.end() ) {
				delete *it;
				DEBUG0(collisions += 1;)
			} else {
				DEBUG1(added+=1;)
				if( (*it) -> isGoal() ) {
					endLoop = true;
					last = *it;
				} else {
					cfgQ.push( *it );
					seen.insert( *it );
				}
			}
		}
		childs.clear();
		DEBUG1(cout<<"added "<<added<<endl;)
	}

	if( !endLoop ) {
		last = cfgQ.front();
	}

	while( last != 0 ) {
		path.push_front( last );
		last = last -> getPrev();
	}
	DEBUG0(cout << "Generated: " << generated << "\n";)
	DEBUG0(cout << "Avoided " << collisions << " collisions" << endl;)
	for( SetCfg::const_iterator sit( seen.begin() ); sit != seen.end(); ++sit ) {
		bool toDel = true;
		for( VecCfg::const_iterator it( path.begin() ); it != path.end(); ++it ) {
			if( *(*sit) == *(*it) ) {
				toDel = false;
				break;
			}
		}
		if( toDel ) {
			delete *sit;
		}
	}
}
