/**
 * File: MagicConfig.h
 *
 * MagicConfig.h specifies the interface to the MagicConfig class.
 *
 * Version:
 * $Id: MagicConfig.h,v 1.2 2013/02/14 01:03:42 acc3863 Exp $
 *
 * Revisions:
 * $Log: MagicConfig.h,v $
 * Revision 1.2  2013/02/14 01:03:42  acc3863
 * bug fixes
 *
 * Revision 1.1  2013/02/13 02:22:01  acc3863
 * Initial revision
 *
 *
 * author: acc3863
 */

#ifndef MAGICCONFIG_H_
#define MAGICCONFIG_H_

#define BOX_SIZE 8
#define TRANS_A 0
#define TRANS_B 1
#define TRANS_C 2
#define OFFSET(x,y,z) x[y] = getData()[z]
#define SAME(x,y) x[y] = getData()[y]

#include "Config.h"
#include <cstring>
#include <iostream>

class MagicConfig : public Config {
public: // init

	MagicConfig( const int* dt, const int* mv, const Config* pr );

	virtual ~MagicConfig();

	static void setGoal( int* g );

public: // util

	virtual void getChildren( std::list< const Config* > &childs ) const;

	virtual bool isGoal() const;

	virtual void print( std::ostream &out ) const;

	virtual int data_length() const;


private:
	static int* GOAL;

};

inline
int MagicConfig::data_length() const {
	return BOX_SIZE;
}

inline
bool MagicConfig::isGoal() const {
	for( int i( 0 ); i < data_length(); ++i )
		if( getData()[i] != GOAL[i] )
			return false;
	return true;
}

inline
void MagicConfig::getChildren( std::list< const Config* > &childs ) const {
	int* d1 = new int[ BOX_SIZE ];
	for( int i( 0 ); i < BOX_SIZE/2; ++i ) {
		OFFSET(d1,i,i+BOX_SIZE/2);
		OFFSET(d1,i+BOX_SIZE/2,i);
	}
	int* m1 = new int[1];
	m1[0] = TRANS_A;

	int* d2 = new int[ BOX_SIZE ];
	OFFSET(d2,0,3);
	OFFSET(d2,1,0);
	OFFSET(d2,2,1);
	OFFSET(d2,3,2);
	OFFSET(d2,4,7);
	OFFSET(d2,5,4);
	OFFSET(d2,6,5);
	OFFSET(d2,7,6);
	int* m2 = new int[1];
	m2[0] = TRANS_B;

	int* d3 = new int[ BOX_SIZE ];
	SAME(d3,0);
	OFFSET(d3,1,5);
	OFFSET(d3,2,1);
	SAME(d3,3);
	SAME(d3,4);
	OFFSET(d3,5,6);
	OFFSET(d3,6,2);
	SAME(d3,7);
	int* m3 = new int[1];
	m3[0] = TRANS_C;

	childs.push_back(new MagicConfig( d1, m1, this ));
	childs.push_back(new MagicConfig( d2, m2, this ));
	childs.push_back(new MagicConfig( d3, m3, this ));
}

#endif // MAGICCONFIG_H_
