/**
 * File: water.cpp main method of the water puzzle
 * 
 * $Id: water.cpp,v 1.15 2013/02/06 19:25:06 scb3968 Exp $
 * 
 * $Log: water.cpp,v $
 * Revision 1.15  2013/02/06 19:25:06  scb3968
 * added comments and checked line lengths
 *
 * Revision 1.14  2013/02/04 23:58:13  acc3863
 * implemented list optimizations discussed in class
 *
 * Revision 1.13  2013/02/04 23:13:26  acc3863
 * fixed calls to new print function
 *
 * Revision 1.12  2013/02/03 20:01:37  scb3968
 * tons of fixes with argument parsing and fixed print to work with ostream,
 * also make new static getters and setters and private data members.
 *
 * Revision 1.11  2013/02/03 18:26:35  scb3968
 * fixed command line input so that it rejects negative bucket sizes.
 *
 * Revision 1.10  2013/01/24 01:08:40  scb3968
 * checked for argc length so that an error could be printed
 *
 * Revision 1.9  2013/01/24 00:33:16  scb3968
 * added description to the header and fixed authoring information
 *
 * Revision 1.8  2013/01/24 00:25:24  scb3968
 * fixed style of code and comment blocks
 *
 * Revision 1.7  2013/01/24 00:08:54  scb3968
 * got rid of commented out code
 *
 * Revision 1.6  2013/01/23 08:18:35  acc3863
 * added istringstream parsing replacing atoi
 *
 * Revision 1.5  2013/01/23 05:49:55  acc3863
 * re-added line breaks
 *
 * Revision 1.4  2013/01/23 05:48:51  acc3863
 * removed move print
 *
 * Revision 1.3  2013/01/18 19:58:16  arch
 * experimented with inline functions
 *
 * Revision 1.2  2013/01/18 02:59:34  achernoff
 * fixed set ordering issue
 *
 * Revision 1.1  2013/01/17 17:54:10  achernoff
 * Initial revision
 *
 */

#include "WaterConfig.h"
#include "Solver.h"
#include <iostream>
#include <sstream>

/// @author achernoff: Alex Chernoff
/// @author sbutton: Scott Button

using namespace std;

int main( int argc, const char* const argv[] ) {
   if( argc >= 3){
	int num(argc - 2); // progname + goal

	int glevel;
	istringstream iss( argv[1] );
	iss >> glevel;
	WaterConfig::setDataMembers(glevel, num);

	//WaterConfig::MAX_LEVELS = new int[WaterConfig::NUM_BUCKETS];
	int* initd = new int[WaterConfig::getNumBuckets()];
	int* initm = new int[2];
	initm[0] = 0;
	initm[1] = 0;
	bool badInput( iss.fail() || !iss.eof() || glevel < 0 );
	for( int i(0); i < WaterConfig::getNumBuckets(); ++i ) {
		int ilevel;
		istringstream issn( argv[i + 2] );
		issn >> ilevel;
		WaterConfig::setValAtIndex(i,ilevel);
		initd[i] = 0;
		badInput = badInput || issn.fail() || !issn.eof() || ilevel<0;
	}
	if( badInput ) {
		cerr << "Invalid argument." << endl <<
			"Usage: {goal fill level} [bucket sizes...]" << endl;
		WaterConfig::deleteMaxLevels();
	} else {
		Config* init = new WaterConfig( initd, initm, 0 );
		Solver solver;
		list< const Config* > path;
		solver.solve( init, path ); 
		if( !path.empty() ) {
			int stepCt( 0 );
			for( list< const Config* >::const_iterator it
				( path.begin());
				it != path.end(); ++it, ++stepCt ) {
				cout << "#" << stepCt << " ";
				(*it) -> print( cout );
				cout << endl;
				delete *it;
			}
		} else {
			cout << "No solution." << endl;
		}
		WaterConfig::deleteMaxLevels();
	}
   }else{
       cerr << "Usage: {goal fill level} [bucket sizes...]" << endl;
   }
   return 0;
}
