/**
 * File: LightConfig.h
 *
 * LightConfig.h specifies the interface to the LightConfig class.
 *
 * Version:
 * $Id: LightConfig.h,v 1.14 2013/02/07 01:31:02 acc3863 Exp $
 *
 * Revisions:
 * $Log: LightConfig.h,v $
 * Revision 1.14  2013/02/07 01:31:02  acc3863
 * fixed bounds error
 *
 * Revision 1.13  2013/02/07 01:19:13  acc3863
 * new macros, spelling
 *
 * Revision 1.12  2013/02/07 01:07:06  scb3968
 * fixed coments
 *
 * Revision 1.11  2013/02/07 00:22:46  acc3863
 * now toggles central light
 *
 * Revision 1.10  2013/02/07 00:19:12  acc3863
 * bug fixes
 *
 * Revision 1.9  2013/02/07 00:13:04  acc3863
 * altered indexing function
 * re-enabled light toggling
 *
 * Revision 1.8  2013/02/04 23:58:13  acc3863
 * implemented list optimizations discussed in class
 *
 * Revision 1.7  2013/02/04 23:18:47  acc3863
 * fixes to print function
 *
 * Revision 1.6  2013/02/03 18:29:37  acc3863
 * inlined functions
 * some optimizations
 *
 * Revision 1.5  2013/01/28 00:04:34  acc3863
 * improved some parts broke others
 *
 * Revision 1.4  2013/01/24 02:41:04  acc3863
 * new changes.
 *
 * Revision 1.3  2013/01/22 00:01:31  acc3863
 * first file reading impl
 *
 * Revision 1.2  2013/01/19 18:49:30  acc3863
 * added bounds checking and refactored some repeated code into functions
 *
 * Revision 1.1  2013/01/18 21:27:24  acc3863
 * Initial revision
 *
 * author: acc3863
 * author: scb3968
 */

#ifndef LIGHTCONFIG_H_
#define LIGHTCONFIG_H_

/// @author achernoff: Alex Chernoff
/// @author sbutton: Scott Button

#include "Config.h"
#include <vector>
#include <istream>
#include <ostream>

/**
 * Models a configuration of the Light puzzle.
 */
class LightConfig : public Config {
public: // init

	/**
	 * Constructs a LightConfig with its current config, used move  
	 * and previous configuration.
	 * @param dt The information the WaterConfig will hold.
	 * @param mv The move made to get to this WaterConfig.
	 * @param pr A pointer to the parent WaterConfig.
	 */
	LightConfig( const int* dt, const int* mv, const Config* pr );

	/**
	 * Destructs a LightConfig.
	 */
	virtual ~LightConfig();

	/**
	* sets the goal number of lights on
	* @param g, the goal number of lights
	*/
	static void setGoal( int g );

public: // util

	/**
	 * Returns a list of pointers to children configurations.
	 * @return a list of pointers to the children configurations.
	 */
	virtual void getChildren( std::list< const Config* > &childs ) const;

	/**
	 * Tests if this configuration is the goal.
	 * @return true if this is the goal.
	 */
	virtual bool isGoal() const;

	/**
	 * Prints this configuration to the specified output stream.
	 * @param stream, the specified stream
	 */
	virtual void print( std::ostream &out ) const;

	/**
	 * Returns the length of this configuration's data array.
	 * @return length of this->getData().
	 */
	virtual int data_length() const;
private: // generating

	/**
	* creates a new config of what would happen when (x,y) is pressed
	* @param x, the x coordinate of the light to be pressed 
	* @param y, the y coordinate of the light to be pressed
	* @return the new config
	*/
	const LightConfig* press( int x, int y ) const;

	/**
	* returns true if its is a valid move, else false
	* @param x, x coordinate of the light to be pressed
	* @param y, y coordinate of the light to be pressed
	* @return if the move is valid
	*/
	bool canPress( int x, int y ) const;

	/**
	* checks if you are within the bounds of the puzzle
	* @param x, x coordinate of the light to be pressed
	* @param y, y coordinate of the light to be pressed
	* @return true if you are in the puzzle's bounds
	*/
	bool boundChk( int x, int y ) const;

public: // stream init 


	static LightConfig* parseStream( std::istream &in );

private: // static members

	/**
	* gives the index of corresponding data to the specified coordinates
	* @param x, x coordinate of the light to be pressed
	* @param y, y coordinate of the light to be pressed
	* @return the index
	*/
	static int index( int x, int y );

	// the width of the puzzle
	static int WIDTH;

	// the height of the puzzle 
	static int HEIGHT;

	// the goal number of lights
	static int GOAL;

};

inline
int LightConfig::index( int x, int y ) {
	return x * WIDTH + y;
}

inline
bool LightConfig::boundChk( int x, int y ) const {
	return 0 <= x && x < HEIGHT && 0 <= y && y < WIDTH;
}

inline
int LightConfig::data_length() const {
	return WIDTH * HEIGHT;
}

inline
void LightConfig::getChildren( std::list< const Config* > &childs ) const {
	for( int i( 0 ); i < HEIGHT; ++i ) {
		for( int j( 0 ); j < WIDTH; ++j ) {
			if( canPress( i, j ) ) {
				childs.push_back( press( i, j ) );
			}
		}
	}
}

inline
bool LightConfig::isGoal() const {
	int lightsOn( 0 );
	for( int i( 0 ); i < data_length(); ++i ) {
		if( getData()[i] >= 1 ) {
			++lightsOn;
		}
		// cut and run
		if( lightsOn > GOAL ) {
			return false;
		}
	}
	return lightsOn == GOAL;
}

#endif // LIGHTCONFIG_H_
