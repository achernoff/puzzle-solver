/**
 * File:
 * $Id: lights.cpp,v 1.6 2013/02/14 02:44:25 acc3863 Exp $
 *
 * Revisions:
 * $Log: lights.cpp,v $
 * Revision 1.6  2013/02/14 02:44:25  acc3863
 * more named constants
 *
 * Revision 1.5  2013/02/07 01:53:15  acc3863
 * print moves
 *
 * Revision 1.4  2013/02/07 01:12:31  acc3863
 * fixed ifstream hanging pointer
 *
 * Revision 1.3  2013/02/04 23:58:13  acc3863
 * implemented list optimizations discussed in class
 *
 * Revision 1.2  2013/02/04 23:18:47  acc3863
 * fixes to print function
 *
 * Revision 1.1  2013/01/28 00:04:34  acc3863
 * Initial revision
 *
 *
 * author: acc3863
 */

#include "LightConfig.h"
#include "Solver.h"
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

int main( int argc, const char* argv[] ) {
	if( argc == 4 ) {
		// setup
		int goalLights;
		istringstream iss1( argv[3] );
		iss1 >> goalLights;
		LightConfig::setGoal( goalLights );
		bool solveReady( goalLights >= 0);
		// streams
		ostream* out( 0 );
		istream* in( 0 );

		ifstream infile;
		ofstream outfile;
		if( *argv[1] == '-' ) { // cin mode
			in = &cin;
		} else { // ifstream mode
			infile.open( argv[1] );
			in = &infile;
			solveReady = solveReady && infile.is_open();
		}
		// output
		if( *argv[2] == '-' ) { // cout mode
			out = &cout;
		} else { // ofstream mode
			outfile.open( argv[2] );
			out = &outfile;
		}
		Solver solver;
		LightConfig* initCfg( LightConfig::parseStream( *in ));
		solveReady = solveReady && !in->bad();
		if( solveReady ) {
			list< const Config* > path;
			solver.solve( initCfg, path );
			if( !path.empty() ) {
				*out << "Solution found." << endl;
				int stepNum( 1 );
				for( list< const Config* >::const_iterator
					it( path.begin() ); it != path.end();
					++it ) {
					if( it != path.begin() ) {
					 *out <<"#" << stepNum++ <<" Press: (";
					 *out <<((*it) -> getMove())[0] << ",";
					 *out <<((*it) -> getMove())[1] << ")";
					 *out << endl;
					} else {
					 *out << "Start at:" << endl;
					}
					(*it) -> print( *out );
					delete *it;
				}
			} else {
				*out << "no solution exists" << endl;
				out -> flush();
			}
		} else {
			cerr << "Usage: ./lights {-/infile} {-/outfile} {# on}"
				<< endl;
		}
		infile.close();
		outfile.close();
	} else {
		cerr << "Invalid arguments." << endl;
		cerr << "Usage: ./lights {-/infile} {-/outfile} {# on}"
			<< endl;
	}
}
