/**
 * $Id: Config.cpp,v 1.6 2013/01/24 00:30:37 acc3863 Exp $
 * 
 * $Log: Config.cpp,v $
 * Revision 1.6  2013/01/24 00:30:37  acc3863
 * standardized documentation
 * fixed RCS format
 * checked line size
 *
 * Revision 1.5  2013/01/18 20:44:57  acc3863
 * successfully implemented inline
 *
 * Revision 1.4  2013/01/18 03:25:33  acc3863
 * added more const types
 *
 * Revision 1.3  2013/01/18 02:59:34  acc3863
 * fixed set ordering issue
 *
 * Revision 1.2  2013/01/17 17:32:09  acc3863
 * type fixes
 * bug fixes
 *
 * Revision 1.1  2013/01/17 16:48:54  acc3863
 * Initial revision
 *
 *
 * author: acc3863
 * author: scb3968
 */

#include "Config.h"
#include <iostream>

Config::Config( const int * dt, const int* mv, const Config * pr ):
	data(dt), move(mv), prev(pr) {

}

Config::~Config() {
	delete[] data;
	delete[] move;
}


