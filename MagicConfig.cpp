/**
 * File: MagicConfig.cpp
 *
 * MagicConfig.cpp implements the MagicConfig class.
 *
 * Version:
 * $Id: MagicConfig.cpp,v 1.2 2013/02/14 01:03:12 acc3863 Exp $
 *
 * Revisions:
 * $Log: MagicConfig.cpp,v $
 * Revision 1.2  2013/02/14 01:03:12  acc3863
 * fixed bugs
 *
 * Revision 1.1  2013/02/13 02:20:42  acc3863
 * Initial revision
 *
 *
 * author: acc3863
 */

#include "MagicConfig.h"

using namespace std;

MagicConfig::MagicConfig( const int* dt, const int* mv, const Config* pr ):
	Config( dt, mv, pr ) {
}

MagicConfig::~MagicConfig() {
}

void MagicConfig::setGoal( int* g ) {
	GOAL = g;
}

void MagicConfig::print( ostream &out ) const {
	out << "[";
	for( int i( 0 ); i < data_length(); ++i ) {
		out << getData()[i];
		if( i == data_length()/2 - 1)
			out << "]" << endl << "[";
		else if( i < data_length() - 1 )
			out << "|";
	}
	out << "]" << endl;
}

int* MagicConfig::GOAL = 0;

