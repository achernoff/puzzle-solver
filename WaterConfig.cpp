/**
 * File: WaterConfig.cpp Config class for the Water puzzle
 * 
 * $Id: WaterConfig.cpp,v 1.13 2013/02/14 02:44:25 acc3863 Exp acc3863 $
 * 
 * $Log: WaterConfig.cpp,v $
 * Revision 1.13  2013/02/14 02:44:25  acc3863
 * more named constants
 *
 * Revision 1.12  2013/02/06 19:25:06  scb3968
 * added comments and checked line lengths
 *
 * Revision 1.11  2013/02/04 23:58:13  acc3863
 * implemented list optimizations discussed in class
 *
 * Revision 1.10  2013/02/03 20:08:27  scb3968
 * included ios
 *
 * Revision 1.9  2013/02/03 20:01:37  scb3968
 * tons of fixes with argument parsing and fixed print to work with
 * ostream,also make new static getters and setters and private data members.
 *
 * Revision 1.8  2013/01/24 01:08:40  scb3968
 * made the printout more user friendly
 *
 * Revision 1.7  2013/01/24 00:33:16  scb3968
 * added description to the header and fixed authoring information
 *
 * Revision 1.6  2013/01/24 00:25:24  scb3968
 * fixed style of code and comment blocks
 *
 * Revision 1.5  2013/01/23 05:54:05  acc3863
 * inlined isGoal and data_length
 *
 * Revision 1.4  2013/01/18 03:25:33  acc3863
 * added more const types
 *
 * Revision 1.3  2013/01/18 03:04:10  acc3863
 * removed unnecessary refs to NUM_BUCKETS
 *
 * Revision 1.2  2013/01/18 02:59:34  acc3863
 * fixed set ordering issue
 *
 * Revision 1.1  2013/01/17 17:54:10  acc3863
 * Initial revision
 *
 */

#include <cstring>
#include <ios>
#include "WaterConfig.h"

#define N_ONE -1
#define ZERO 0
#define ONE 1
#define TWO 2

/// @author achernoff: Alex Chernoff
/// @author sbutton: Scott Button

using namespace std;

WaterConfig::WaterConfig( const int* dt, const int* mv, const Config* pr ):
	Config( dt, mv, pr ) {

}

WaterConfig::~WaterConfig() {

}

void WaterConfig::getChildren( list< const Config* > &childs ) const {
	for( int i(N_ONE); i < data_length(); ++i )
		for( int j(N_ONE);j < data_length(); ++j )
			if( i != j && canPour( i, j ) )
				childs.push_back( pour( i, j ) );
}

void WaterConfig::print( ostream& stream ) const {
	for( int i(ZERO); i < data_length(); ++i ) {
		stream << "[" << getData()[i] << "/" << MAX_LEVELS[i] << "]";
	}
}

void WaterConfig::setDataMembers( int goal, int numb ){
	MAX_LEVELS = new int[numb];
	GOAL_LEVEL = goal;
	NUM_BUCKETS = numb;
}

int WaterConfig::getValAtIndex( int index ){
	return MAX_LEVELS[index];	
}

void WaterConfig::setValAtIndex( int index, int val ){
	MAX_LEVELS[index] = val;
} 

int WaterConfig::getNumBuckets(){
	return NUM_BUCKETS;
}

void WaterConfig::deleteMaxLevels(){
	delete[] MAX_LEVELS;
}

bool WaterConfig::canPour( int src, int dest ) const { 
	if( src != N_ONE && dest != N_ONE ) { // bucket to bucket
		return getData()[dest] < MAX_LEVELS[dest] && getData()[src]>ZERO;
	} else if( src == N_ONE && dest != N_ONE ) { // src to bucket
		return getData()[dest] < MAX_LEVELS[dest];
	} else if( src != N_ONE && dest == N_ONE ) { // bucket to src
		return getData()[src] > ZERO;
	} else { // nonsense src to src no real action
		return false;
	}
}

const WaterConfig* WaterConfig::pour( int src, int dest ) const {
	int* nd( new int[data_length()] );
	int* mv( new int[TWO] );
	memcpy( nd, getData(), data_length() * sizeof( int ) );
	mv[ZERO] = src;
	mv[ONE] = dest;
	if( src != N_ONE && dest != N_ONE ) { // bucket to bucket
		int max = getData()[src] < MAX_LEVELS[dest] - getData()[dest] ?
			getData()[src] : MAX_LEVELS[dest] - getData()[dest];
		nd[src]  -= max;
		nd[dest] += max;
		return new WaterConfig( nd, mv, this );
	} else if ( src == N_ONE && dest != N_ONE ) { // src to bucket
		nd[dest] = MAX_LEVELS[dest];
		return new WaterConfig( nd, mv, this );
	} else if ( src != N_ONE && dest == N_ONE ) { // bucket to src
		nd[src] = ZERO;
		return new WaterConfig( nd, mv, this );
	} else { // nonsense src to src no real action
		return new WaterConfig( nd, mv, this );
	}
}

int* WaterConfig::MAX_LEVELS = ZERO;

int WaterConfig::GOAL_LEVEL = ZERO;

int WaterConfig::NUM_BUCKETS = ZERO;
