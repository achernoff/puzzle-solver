/*
 * File: Solver.h 
 * 
 * Solver.h defines the Solver class interface.
 * 
 * Version: 
 * $Id: Solver.h,v 1.8 2013/02/04 23:58:13 acc3863 Exp $ 
 * 
 * Revisions: 
 * $Log: Solver.h,v $
 * Revision 1.8  2013/02/04 23:58:13  acc3863
 * implemented list optimizations discussed in class
 *
 * Revision 1.7  2013/01/24 00:30:37  acc3863
 * standardized documentation
 * fixed RCS format
 * checked line size
 *
 * Revision 1.6  2013/01/18 19:34:58  acc3863
 * added documentation
 *
 * Revision 1.5  2013/01/18 03:25:33  acc3863
 * added more const types
 *
 * Revision 1.4  2013/01/18 02:59:34  acc3863
 * fixed set ordering issue
 *
 * Revision 1.3  2013/01/17 17:32:09  acc3863
 * type fixes
 * bug fixes
 *
 * Revision 1.2  2013/01/17 16:48:54  acc3863
 * initial implementation
 *
 * Revision 1.1  2013/01/17 15:31:19  acc3863
 * Initial revision
 * 
 * author: acc3863
 * author: scb3968
 */

#ifndef SOLVER_H_
#define SOLVER_H_

#include <list>
#include "Config.h"

/**
 * Solver class describes the algorithm used to solve a range of shortest-route
 * problems.
 */
class Solver {
public:

	/**
	 * Constructs a Solver.
	 */
	Solver();

	/**
	 * Destructs a Solver.
	 */
	~Solver();

	/**
	 * Solves a shortest-route problem given an initial configuration.
	 * @param init the initial configuration.
	 * @param path A reference to a list storing pointers to the in-order
	 	solution path.
	 */
	void solve( const Config* init, std::list< const Config* > &path ) const;
};

#endif  //SOLVER_H_
