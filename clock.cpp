/**
 * $Id: clock.cpp,v 1.17 2013/02/06 19:25:06 scb3968 Exp $
 * 
 * $Log: clock.cpp,v $
 * Revision 1.17  2013/02/06 19:25:06  scb3968
 * added comments and checked line lengths
 *
 * Revision 1.16  2013/02/06 18:15:40  scb3968
 * changed data members to private and wrote setters and getters 
 * for each function.
 *
 * Revision 1.15  2013/02/04 23:58:13  acc3863
 * implemented list optimizations discussed in class
 *
 * Revision 1.14  2013/02/04 23:13:38  acc3863
 * fixed calls to new print function
 *
 * Revision 1.13  2013/01/24 00:30:37  acc3863
 * standardized documentation
 * fixed RCS format
 * checked line size
 *
 * Revision 1.12  2013/01/24 00:16:08  acc3863
 * new typedef to shorten line length
 *
 * Revision 1.11  2013/01/23 08:20:18  acc3863
 * removed unnecessary include
 *
 * Revision 1.10  2013/01/23 06:01:50  acc3863
 * added new step counter prefix
 *
 * Revision 1.9  2013/01/22 22:16:29  acc3863
 * merged 2 if statements into one huge monster
 *
 * Revision 1.8  2013/01/21 19:35:04  acc3863
 * cleaned up code style
 *
 * Revision 1.7  2013/01/21 00:56:58  scb3968
 * fully working with istringstreams checking for bad input in all cases
 *
 * Revision 1.6  2013/01/18 21:05:33  scb3968
 * fixed output typo.
 *
 * Revision 1.5  2013/01/18 20:54:50  scb3968
 * forgot to include the header file for stringstream
 *
 * Revision 1.4  2013/01/18 20:52:51  scb3968
 * added bad input checks via istringstream
 *
 * Revision 1.3  2013/01/17 18:15:34  acc3863
 * refixed bounds bug
 *
 * Revision 1.2  2013/01/17 17:32:09  acc3863
 * type fixes
 * bug fixes
 *
 * Revision 1.1  2013/01/17 16:48:54  acc3863
 * Initial revision
 *
 *
 * author: acc3863
 * author: scb3968
 */

#include "Solver.h"
#include "ClockConfig.h"
#include <iostream>
#include <sstream>
#include <list>

using namespace std;

typedef list< const Config* > VecCfg;

int main( int argc, const char* argv[] ) {
	if(argc == 4) {
		int clk_size;
		istringstream iss( argv[1] );
		iss >> clk_size;
		ClockConfig::setClockSize( clk_size );
		
		int clk_start;
		istringstream iss1(argv[2]);
		iss1 >> clk_start;
		
		int goal;
		istringstream iss2(argv[3]);
		iss2 >> goal;
		ClockConfig::setGoalHour( goal );

		if (iss.fail() || iss1.fail() || iss2.fail() || !iss.eof()
		|| !iss1.eof() || !iss2.eof()
		|| ClockConfig::getClockSize() < 1
		|| clk_start < 1
		|| clk_start > ClockConfig::getClockSize()
		|| ClockConfig::getGoalHour() < 1
		|| ClockConfig::getGoalHour() > ClockConfig::getClockSize() ) {
			cerr << "Invalid argument." << endl <<
				"Usage: {clock size} {start} {end}" << endl;
		} else {
			int* sd( new int[1] );
			int* sm( new int[1] );
			sd[0] = clk_start;
			Config* start = new ClockConfig(sd, sm, 0);
			Solver solver;
			VecCfg path;
			solver.solve( start, path );
			if( !path.empty() ) {
				int stepNum( 1 );
				for( VecCfg::const_iterator it = path.begin();
					it != path.end(); ++it) {
					if( it == path.begin() ) {
						cout << "Starting at ";
						(*it) -> print( cout );
						cout << "," << endl;
					} else {
						cout << "#" << stepNum
							<< ": move ";
						if( (*it) -> getMove()[0] ) {
							cout << "backwards";
						} else {
							cout << "forwards";
						}

						if( it == (--path.end()) ) {
							cout << ", end at ";
						} else {
							cout << " to ";
						}
						(*it) -> print( cout );
						cout << endl;
						stepNum += 1;
					}
					delete *it;
				}
			} else {
				cout << "No solution found." << endl;
			}
		}
	} else {
		cerr << "Usage: {clock size} {start} {end}" << endl;
	}
	return 0;
}
