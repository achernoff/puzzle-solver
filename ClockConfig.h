/**
 * File: ClockConfig.h 
 * 
 * ClockConfig.h defines the interface of the ClockConfig class.
 * 
 * Version: 
 * $Id: ClockConfig.h,v 1.11 2013/02/06 19:25:06 scb3968 Exp $ 
 * 
 * Revisions: 
 * $Log: ClockConfig.h,v $
 * Revision 1.11  2013/02/06 19:25:06  scb3968
 * added comments and checked line lengths
 *
 * Revision 1.10  2013/02/06 18:15:40  scb3968
 * changed data members to private and wrote setters and getters 
 * for each function.
 *
 * Revision 1.9  2013/02/04 23:58:13  acc3863
 * implemented list optimizations discussed in class
 *
 * Revision 1.8  2013/02/04 23:18:47  acc3863
 * fixes to print function
 *
 * Revision 1.7  2013/01/24 00:30:37  acc3863
 * standardized documentation
 * fixed RCS format
 * checked line size
 *
 * Revision 1.6  2013/01/24 00:17:06  acc3863
 * edited documentation to shorten line length
 *
 * Revision 1.5  2013/01/23 05:45:25  acc3863
 * more inline
 *
 * Revision 1.4  2013/01/18 20:44:57  acc3863
 * successfully implemented inline
 *
 * Revision 1.3  2013/01/18 19:33:41  acc3863
 * added documentation
 *
 * Revision 1.2  2013/01/17 17:32:09  acc3863
 * type fixes
 * bug fixes
 *
 * Revision 1.1  2013/01/17 16:48:54  acc3863
 * Initial revision
 *
 * author: acc3863
 * author: scb3968
 */

#ifndef CLOCKCONFIG_H_
#define CLOCKCONFIG_H_

#include "Config.h"
#include <ios>
/**
 * ClockConfig class modeling a clock's state and possible children
 * configurations.
 */
class ClockConfig: public Config {
public:
	/**
	 * Constructs a clock with a time, direction, and previous
	 * 	Config pointer.
	 * @param dt An array of 1 integer, the hour of the clock.
	 * @param mv An array of 1 integer indicated which direction the clock
		was moved.
	 * @param pr The previous ClockConfig
	 */
	ClockConfig( int* dt, int* mv, const Config* pr );

	/**
	 * Destructs a clock configuration.
	 */
	virtual ~ClockConfig();

	/**
	 * Generates pointers to children configurations.
	 * @param childs A list to store pointers to the children configs.
	 */
	virtual void getChildren( std::list< const Config* > &childs ) const;

	/**
	 * Tests if this configuration is the goal.
	 * @return true if this is the goal.
	 */
	virtual bool isGoal() const;

	/**
	 * Prints this configuration to an output stream.
	 * @param out the output stream.
	 */
	virtual void print( std::ostream &out ) const;

	/**
	 * Returns the length of this configuration's data array.
	 * @return length of this->getData().
	 */
	virtual int data_length() const;

	/**
	* Returns the size of the clock
	* @return CLOCK_SIZE
	*/
	static int getClockSize();
	
	/**
	* sets CLOCK_SIZE
	* @param size the size of the clock
	*/
	static void setClockSize( int size );

	/**
	* returns GoalHour
	* @return GOAL_HOUR
	*/
	static int getGoalHour();

	/**
	* sets GOAL_HOUR
	* @param goalHour the time you are trying to get set to
	*/
	static void setGoalHour( int goalHour );

private: //private data members

	/**
	 * The goal hour to turn the clock to.
	 */
	static int GOAL_HOUR;

	/**
	 * The maximum numerical hour on the clock.
	 */
	static int CLOCK_SIZE;
};

inline
bool ClockConfig::isGoal() const {
	return *getData() == GOAL_HOUR;
}

inline
int ClockConfig::data_length() const {
	return 1;
}

#endif // CLOCKCONFIG_H_

