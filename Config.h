/*
 * File: Config.h 
 * 
 * Config.h defines the Config class interface.
 * 
 * Version: 
 * $Id: Config.h,v 1.14 2013/02/04 23:58:13 acc3863 Exp $ 
 * 
 * Revisions: 
 * $Log: Config.h,v $
 * Revision 1.14  2013/02/04 23:58:13  acc3863
 * implemented list optimizations discussed in class
 *
 * Revision 1.13  2013/02/04 23:18:47  acc3863
 * fixes to print function
 *
 * Revision 1.12  2013/02/03 19:21:57  acc3863
 * ouput stream reference
 *
 * Revision 1.11  2013/02/03 19:12:05  acc3863
 * print now outputs to any output stream
 *
 * Revision 1.10  2013/01/24 00:30:37  acc3863
 * standardized documentation
 * fixed RCS format
 * checked line size
 *
 * Revision 1.9  2013/01/18 20:44:57  acc3863
 * successfully implemented inline
 *
 * Revision 1.8  2013/01/18 20:13:46  acc3863
 * removed stray inline
 *
 * Revision 1.7  2013/01/18 19:56:10  acc3863
 * experimented with inline functions
 *
 * Revision 1.6  2013/01/18 19:34:58  acc3863
 * added documentation
 *
 * Revision 1.5  2013/01/18 03:25:33  acc3863
 * added more const types
 *
 * Revision 1.4  2013/01/18 02:59:34  acc3863
 * fixed set ordering issue
 *
 * Revision 1.3  2013/01/17 17:32:09  acc3863
 * type fixes
 * bug fixes
 *
 * Revision 1.2  2013/01/17 16:48:54  acc3863
 * initial implementation
 *
 * Revision 1.1  2013/01/17 15:31:19  acc3863
 * Initial revision
 * 
 * author: acc3863
 * author: scb3968
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include <list>
#include <ostream>

/**
 * The Config class describes a configuration in the solution path for a
 * shortest-route problem.
 */
class Config {
public: // initialization

	/**
	 * Constructs a configuration, initializing all its fields.
	 * @param dt The information the configuration will hold.
	 * @param mv The move that was made to get to this configuration.
	 * @param pr A pointer to the parent configuration.
	 */
	Config( const int* dt, const int* mv, const Config* pr );

	/**
	 * Destructs a configuration.
	 */
	virtual ~Config();

public: // member getters

	/**
	 * Returns the data member of this configuration.
	 * @return the data member.
	 */
	const int* getData() const;

	/**
	 * Returns the move member of this configuration.
	 * @return the move member.
	 */
	const int* getMove() const;

	/**
	 * Returns the prev member of this configuration.
	 * @return the prev member.
	 */
	const Config* getPrev() const;

public: // operators
	
	/**
	 * Tests if this configuration is equal to another.
	 * @param o A configuration to test against.
	 * @return true if the configurations are equal.
	 */
	bool operator==( const Config &o ) const;

	/**
	 * Tests if this configuration is unequal to another.
	 * @param o A configuration to test against.
	 * @return true if the configurations are unequal.
	 */
	bool operator!=( const Config &o ) const;

	/**
	 * Tests if this configuration is less than another.
	 * @param o A configuration to test against.
	 * @return true if this configuration is less than o.
	 */
	bool operator<( const Config &o ) const;

public: // utility

	/**
	 * Generates any children configurations this configuration can produce
	 * on the heap.
	 * @return A vector containing pointers to the children configurations.
	 */
	virtual void getChildren( std::list< const Config* > &childs ) const = 0;

	/**
	 * Tests if this configuration meets the goal condition.
	 * @return true if the goal is met.
	 */
	virtual bool isGoal() const = 0;

	/**
	 * Prints this configuration to the passed output stream.
	 * @param out The output stream.
	 */
	virtual void print( std::ostream &out ) const = 0;

	/**
	 * Returns the length of this configuration's data member.
	 * @return this.data's length.
	 */
	virtual int data_length() const = 0;

private: // data members

	/**
	 * An integer pointer to an array used to describe this configuration.
	 */
	const int* const data;

	/**
	 * The move used to get to this configuration, unused if prev is NULL.
	 */
	const int* const move;

	/**
	 * The pointer to this configuration that generated this configuration.
	 * May be NULL if this is the initial configuration.
	 */
	const Config* const prev;
};

inline
const int* Config::getData() const {
	return data;
}

inline
const int* Config::getMove() const {
	return move;
}

inline
const Config* Config::getPrev() const { 
	return prev;
}

inline
bool Config::operator==( const Config &other ) const {
	for( int i(0); i < data_length(); ++i ) {
		if( data[i] != other.data[i] ) {
			return false;
		}
	}
	return true;
}

inline
bool Config::operator!=( const Config &other ) const {
	for( int i(0); i < data_length(); ++i ) {
		if( data[i] == other.data[i] ) {
			return false;
		}
	}
	return true;
}

inline
bool Config::operator<( const Config &other ) const {
	for( int i( 0 ); i < data_length(); ++i ) {
		if( data[i] < other.data[i] ) {
			return true;
		} else if ( data[i] > other.data[i] ) {
			return false;
		}
	}
	return false;
}

#endif // CONFIG_H_
