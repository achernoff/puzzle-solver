/** 
* File: WaterConfig.h 
* 
* WaterConfig.h header file for the WaterConfig class
* 
* Version: 
* $Id: WaterConfig.h,v 1.13 2013/02/07 01:07:06 scb3968 Exp $ 
* 
* Revisions: 
* $Log: WaterConfig.h,v $
* Revision 1.13  2013/02/07 01:07:06  scb3968
* fixed coments
*
* Revision 1.12  2013/02/06 19:25:06  scb3968
* added comments and checked line lengths
*
* Revision 1.11  2013/02/04 23:58:13  acc3863
* implemented list optimizations discussed in class
*
* Revision 1.10  2013/02/04 23:18:47  acc3863
* fixes to print function
*
* Revision 1.9  2013/02/03 20:08:27  scb3968
* included ios
*
* Revision 1.8  2013/02/03 20:01:37  scb3968
* tons of fixes with argument parsing and fixed print to work with ostream,also make new static getters and setters and private data members.
*
* Revision 1.7  2013/01/24 00:33:16  scb3968
* added description to the header and fixed authoring information
*
* Revision 1.6  2013/01/24 00:25:24  scb3968
* fixed style of code and comment blocks
*
* Revision 1.5  2013/01/23 05:54:05  acc3863
* inlined isGoal and data_length
*
* Revision 1.4  2013/01/18 19:34:58  acc3863
* added documentation
*
* Revision 1.3  2013/01/18 03:25:33  acc3863
* added more const types
*
* Revision 1.2  2013/01/18 03:00:24  acc3863
* fixed set ordering issue
*
* Revision 1.1  2013/01/17 17:54:10  acc3863
* Initial revision
* 
*/ 


#ifndef WATERCONFIG_H_
#define WATERCONFIG_H_

/// @author achernoff: Alex Chernoff
/// @author sbutton: Scott Button

#include "Config.h"
#include <ios>

/**
 * WaterConfig class modeling a set of water bucket's state and possible
 * children configurations.
 */
class WaterConfig : public Config {
public: //init

	/**
	 * Constructs a WaterConfig with its bucket's fill amounts, used move  
	 * and previous configuration.
	 * @param dt The information the WaterConfig will hold.
	 * @param mv The move made to get to this WaterConfig.
	 * @param pr A pointer to the parent WaterConfig.
	 */
	WaterConfig( const int* dt, const int* mv, const Config* pr );

	/**
	 * Destructs a WaterConfig.
	 */
	virtual ~WaterConfig();

public: //utility

	/**
	 * Returns a list of pointers to children configurations.
	 * @return a list of pointers to the children configurations.
	 */
	virtual void getChildren( std::list< const Config* > &childs ) const;

	/**
	 * Tests if this configuration is the goal.
	 * @return true if this is the goal.
	 */
	bool isGoal() const;

	/**
	 * Prints this configuration to the specified output stream.
	 * @param stream, the specified stream
	 */
	void print( std::ostream& stream ) const;

	/**
	 * Returns the length of this configuration's data array.
	 * @return length of this->getData().
	 */
	int data_length() const;

	/**
	* setter method for the data members used in the water puzzle
	* @param GOAL, goal level of water to have
	* @param NUM_B, the total number of buckets
	*/
	static void setDataMembers( int goal, int numb );
	
	/**
	* @param index the index of the data
	* @return the value at the index
	*/
	static int getValAtIndex( int index );

	/**
	* @param index the index of the data
	* @param val the value of the data to be put in the array
	*/
	static void setValAtIndex( int index, int val ); 
	
	/**
	* @return the number of buckets
	*/
	static int getNumBuckets();
	
	/**
	* delets the array
	*/
	static void deleteMaxLevels();

private: //children generating

	/**
	 * Tests if a bucket can pour into another.
	 * -1 in the src parameter indicates filling from the water source.
	 * -1 in the dest parameter indicates pouring the water out to nothing.
	 * @param src the source bucket
	 * @param dest the destination bucket
	 * @return true if pouring src to dest would change the configuration.
	 */
	bool canPour( int src, int dest ) const;

	/**
	 * Performs a pour from one bucket to another, returning 
	 * a pointer to the resulting configuration.
	 * -1 in the src parameter indicates filling from the water source.
	 * -1 in the dest parameter indicates pouring the water out to nothing.
	 * @param src the source bucket
	 * @param dest the destination bucket
	 * @return A pointer to the resulting configuration.
	 */
	const WaterConfig* pour( int src, int dest ) const;

private: //private data members

	/**
	 * An array of integers describing each bucket's maximum capacity.
	 */
	 static int* MAX_LEVELS;

	/**
	 * The goal water level in any bucket.
	 */
	 static int GOAL_LEVEL;

	/**
	 * The total number of buckets.
	 */
	 static int NUM_BUCKETS;
};

inline
int WaterConfig::data_length() const {
	return NUM_BUCKETS;
}

inline
bool WaterConfig::isGoal() const {
	for( int i( 0 ); i < data_length(); ++i ) {
		if( getData()[i] == GOAL_LEVEL ) {
			return true;
		}
	}
	return false;
}

#endif // WATERCONFIG_H_

